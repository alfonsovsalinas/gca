<?php
/*
 * html.php
 * 
 * Copyright 2019 alfonsovsalinas <alfonsovsalinas@gmail.com>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */

error_reporting(E_ALL|E_STRICT);

class HtmlClass {

//private $bulmadir = 'https://cdnjs.cloudflare.com/ajax/libs/bulma/0.7.5/css/bulma.min.css';
private $bulmadir	= '../../vendor/bulma/0.9.1/css/bulma.min.css';
private $jquerydir	= '../../vendor/jquery/3.4.1/jquery.min.js';
private $fontdir	= '../../vendor/fontawesome/5.12.1/css/all.css';
//private $fontdir	= 'https://use.fontawesome.com/releases/v5.3.1/js/all.js';

/* -----------------------------------------------------------------------------------------------------------------------------------------
 * Component body	-> cuerpo del html 
 * @param	-> ($ini=0,$titulo=null)
 * $ini		-> 1=abre el componente, 0=cierra el componente
 * $mdf	-> titulo de la página
 * -------------------------------------------------------------------------------------------------------------------------------------- */
public function body($ini=0,$titulo=null) {
	if ($ini==1) 
		$res= "
		<!DOCTYPE html><html>
		<head>
			<meta charset='utf-8'>
			<title>$titulo</title>
			<meta name='viewport' content='width=device-width, initial-scale=1'>
			<meta name='mobile-web-app-capable' content='yes'> 
			<link rel='icon' sizes='192x192' href='/icon.png'>
			<link rel='stylesheet' type='text/css' href='{$this->bulmadir}' />
			<link href='{$this->fontdir}' rel='stylesheet'>			
			<link rel='stylesheet' type='text/css' href='css/screen.css'/>
			<style>
				@media screen and (max-width: 768px) {
					#menu-toggle:checked + .nav-menu { display: block; }
				}
			</style>	
		</head>
		<body>
		";
	else 
		$res="</body></html>";
	echo $res;
}

/* -----------------------------------------------------------------------------------------------------------------------------------------
 * Component FButton
 * @param -> ($pref=0,$mdfbut='is-info',$tex,$attbut)
 * $pref	-> 	 0: boton simple con un control de formulario con declaración de un solo campo en el formulario;
 * 				 2: boton simple con un control de formulario con declaración de campo agrupado (aplica al primer boton, los demás del grupo son simples con control)
 * 				 1: botón simple con un control de formulario en un campo agrupado sin cierre de campo agrupado
 * 				10: botón simple con un control de formulario y cierre de campo agrupado
 * $mdfbut	-> modificadores del boton, ej. 'is-info is-fullwidth'
 * $tex		-> el texto en el botón
 * $attbut	-> atributos adicionales al boton, ej. "id='fb_login'"
 * -------------------------------------------------------------------------------------------------------------------------------------- */
public function fbutton($pref=0,$mdfbut='is-info',$tex,$attbut) {
	switch ($pref) {
		case 1 : $res="
						<div class='control'>
							<button class='button $mdfbut' $attbut>$tex</button>
						</div>
					"; 
		case 2 : $res="
					<div class='field is-grouped'>
						<div class='control'>
							<button class='button $mdfbut' $attbut>$tex</button>
						</div>
					"; 
		case 10: $res="
						<div class='control'>
							<button class='button $mdfbut' $attbut>$tex</button>
						</div>
					</div>
					"; 
		default: $res="
					<div class='field'>
						<div class='control'>
							<button class='button $mdfbut' $attbut>$tex</button>
						</div>
					</div>
					"; 
	}
	echo $res;
}

/* -----------------------------------------------------------------------------------------------------------------------------------------
 * Component Columns
 * @param -> ($ini=1,$section=0,$mdfcol=null,$attsec=null)
 * $ini			-> 1=abre el componente, 0=cierra el componente
 * $section		-> 1=dentro de una sección definición columns, 0=unicamente el column
 * $mdfcol		-> propiedades del componente column
 * $attsec		-> propiedades del componente section
 * -------------------------------------------------------------------------------------------------------------------------------------- */
public function column($ini=1,$section=0,$mdfcol=null,$attsec=null) {
	if ($ini==1) 
		if ($section==1) $res="
			<section class='section' $attsec >
				<div class='columns $mdfcol'>
			";
		else
			$res="<div class='column $mdfcol'>"; 
	else 
		if ($section==1) 
			$res="</div></section>";
		else
			$res="</div>";
	echo $res;	
}

/* -----------------------------------------------------------------------------------------------------------------------------------------
 * Component Container	->	https://bulma.io/documentation/layout/container/
 * @param 	-> ($ini=0,$section=0,$mdfcon=null,$mdfsec=null)
 * $ini		-> 1=abre el componente, 0=cierra el componente
 * $section	-> 1=dentro de una sección, 0=unicamente el container
 * $mdfcon	-> propiedades del componente container
 * $mdfsec	-> propiedades del componente section
 * -------------------------------------------------------------------------------------------------------------------------------------- */
public function container($ini=0,$section=0,$mdfcon=null,$mdfsec=null) {
	if ($ini==1)
		if ($section==1) 
			$res="
				<section class='section $mdfsec'>
					<div class='container $mdfcon'>
			";
		else
			$res="<div class='container $mdfcon'>";
	else
		if ($section==1) 
			$res="</section></div>"; 
		else
			$res="</div>";
	echo $res;
}

/* -----------------------------------------------------------------------------------------------------------------------------------------
 * Component Form Input	->	https://bulma.io/documentation/form/input/
 * @param	-> 	($prms)
 * $prms	->	arreglo de parámetros
 * 				array(
 * 				"prefijo"	=> "<div class='level-item'>",
 *				"label"		=> "<label class='label'>campo</label>",
 *				"iconleft"	=> "fa-user",
 *				"iconright"	=> "fa-check",
 *				"input" 	=> "<input class='input is-black' type='text' id='1' placeholder='escribe algo' value='' autocomplete='new-password'>",  //dígale a Chrome que esta es una nueva entrada de contraseña y no proporcionará las antiguas como sugerencias de autocompletar.
 *				"button"	=> "<button class='button is-black'>Buscar</button>",
 *				"help" 		=> "<p class='help is-black'>información de ayuda para el campo</p>" );			
 * -------------------------------------------------------------------------------------------------------------------------------------- */
public function input($prm) {
	$res=null;
	if (isset($prm['prefijo'])) $res=$prm['prefijo'];
	if (isset($prm['button'])) $res=$res."<div class='field has-addons'>"; else $res=$res."<div class='field'>";					
	$res=$res.$prm['label'];
	$res=$res."<div class='control";
	if (isset($prm['iconleft']))  $res=$res." has-icons-left";
	if (isset($prm['iconright'])) $res=$res." has-icons-right";
	$res=$res."'>";
	$res=$res.$prm['input'];
	if (isset($prm['iconleft']))  $res=$res."<span class='icon is-small is-left'><i class='fas {$prm['iconleft']}'></i></span>";
	if (isset($prm['iconright'])) $res=$res."<span class='icon is-small is-right'><i class='fas {$prm['iconright']}'></i></span>";
	$res=$res."</div>";
	if (isset($prm['button'])) $res=$res."<p class='control'>{$prm['button']}</p>";
	$res=$res.$prm['help'];
	$res=$res."</div>";
	if (isset($prm['prefijo'])) $res=$res."</div>";
	echo $res;
}

/* -----------------------------------------------------------------------------------------------------------------------------------------
 * Librerías Javascript
 * @param 	-> ($js,$uni=0)
 * $js		-> declaración de uso libreria javascript ubicado en la carpeta js 
 * $uni		-> 0: carga librerías requeridas;  1: solo una librería, la declarada en $js
 * -------------------------------------------------------------------------------------------------------------------------------------- */
public function js($js,$uni=0) {
	if ($uni==0) 
		$res="<script type='text/javascript' src='{$this->jquerydir}'></script>
			  <script type='text/javascript' src='js/".$js."'></script>
			 ";
	else
		$res="<script type='text/javascript' src='js/".$js."'></script>";	
	echo $res;
}

/* -----------------------------------------------------------------------------------------------------------------------------------------
 * Component Modal	->	https://bulma.io/documentation/components/modal/
 * @param 	-> ($mdfcol)
 * $mdfcon	-> modificador de color del componente
 * -------------------------------------------------------------------------------------------------------------------------------------- */
public function modal() {
	$res="
	<div class='modal' id='omodal'>
		<div class='modal-background'></div>
		<div class='modal-content'>

			<article class='message is-dark' id='omessage'>
				<div class='message-header'><p id='headmessage'>Danger</p>
					<button class='delete' aria-label='delete' id='bdeletemessage'></button>				
				</div>
				<div class='message-body'><p id='bodymessage'></p></div>
			</article>

		</div>
	</div>
	";
	echo $res;
}

/* -----------------------------------------------------------------------------------------------------------------------------------------
 * Component navbar -> https://bulma.io/documentation/components/navbar/
 * @param -> ($ini=1,$component='main',$text=null,$mdf=null,$id=null)
 * $ini			-> 1=abre el componente, 0=cierra el componente
 * $component	-> el nombre del componente
 * $text		-> texto que acompaña el componente
 * $mdf		-> propiedades del componente
 * $id			-> identificador del componente
 * -------------------------------------------------------------------------------------------------------------------------------------- */
public function navbar($ini=1,$component='main',$text=null,$mdf=null,$id=null) {
	if ($ini==1)
		switch ($component) {
			case 'main' : $res="<nav class='navbar $mdf' role='navigation' aria-label='main navigation'>"; break;
			case 'brand': $res="<div class='navbar-brand'>"; break;
			case 'img'  : $res="
				<a class='navbar-item'>
					<img src='images/".$text."'>
				</a>
				"; break;
			case 'burger': $res="
				<a role='button' class='navbar-burger burger' aria-label='menu' aria-expanded='false' data-target='navbarBasicExample'>
					<span aria-hidden='true'></span>
					<span aria-hidden='true'></span>
					<span aria-hidden='true'></span>
				</a>
				"; break;
			case 'menu' : $res="<div id='navbarBasicExample' class='navbar-menu'>"; break;
			case 'start': $res="<div class='navbar-start'>"; break;
			case 'item' : $res="<a class='navbar-item $mdf' id='$id'>$text</a>"; break;
			case 'link' : $res="
				<div class='navbar-item has-dropdown is-hoverable'>
					<a class='navbar-link'>$text</a>
					<div class='navbar-dropdown'>
				"; break;
			case 'divider' : $res="<hr class='navbar-divider'>"; break;
			case 'end'	   : $res="<div class='navbar-end'>"; break;
			case 'buttons' : $res="
				<div class='navbar-item'>
					<div class='buttons'>
				"; break;
			case 'button'  : $res="<a class='button $mdf'><strong>$text</strong></a>"; break;
			case 'tags' : $res="
				<div class='tags has-addons'>
				"; break;
			case 'tag'  : $res="<span class='tag $mdf'>$text</span>"; break;
			default: return null;
		}	
	else 
		switch ($component) {
			case 'main':  $res="</nav>"; break;
			case 'menu' :
			case 'start':
			case 'end':
			case 'brand':
			case 'tags': $res="</div>"; break;
			case 'buttons':
			case 'link' : $res="</div></div>"; break;
			default: return null;
		}	
	echo $res;			
}

/* -----------------------------------------------------------------------------------------------------------------------------------------
 * Component Notification	->	https://bulma.io/documentation/elements/notification/
 * @param 	-> ($mdfcol)
 * $mdfcon	-> modificador de color del componente
 * -------------------------------------------------------------------------------------------------------------------------------------- */
public function notifica($mdfcol='is-info is-light') {
	$res="
	<div class='notification $mdfcol' id='onotifica' style='display: none;'>
		<button class='delete' id='bdeletenotifica'></button><p id='otexnotifica'></p>
	</div>
	";
	echo $res;
}

/* -----------------------------------------------------------------------------------------------------------------------------------------
 * Component Subtitle	->	https://bulma.io/documentation/elements/title/
 * @param 	-> ($tex,$mdf=null)
 * $tex		-> el texto 
 * $mdf		-> el modificador subtitle
 * -------------------------------------------------------------------------------------------------------------------------------------- */
public function subtitle($tex,$mdf=null) {
	$res="<p class='subtitle $mdf'>$tex</p>";
	echo $res;
}

/* -----------------------------------------------------------------------------------------------------------------------------------------
 * Component Subtitle	->	https://bulma.io/documentation/elements/title/
 * @param 	-> ($tex,$mdf=null)
 * $tex		-> el texto 
 * $mdf		-> el modificador title
 * -------------------------------------------------------------------------------------------------------------------------------------- */
public function title($tex,$mdf=null) {
	$res="<p class='title $mdf'>$tex</p>";
	echo $res;
}

//----------------------------------------------------------------------------------------------------------------------------------------------------------------------	
}
?>
