//----------------------------------------------------------------------------------------------------------------------------------------------------------------------
var api = {
	pathapi		:	'http://localhost/git/swei/api/api.php?urlget=',
	url			:	null,
	post		:	null
};

//----------------------------------------------------------------------------------------------------------------------------------------------------------------------
var fe = {
	coordx		: 0,
	coordy		: 0,
	modal		:	function(pcab,pmensaje,pcol='is-light',) {
						$('#omodal').attr('class','modal is-active');
						$('#headmessage').html(pcab);
						$('#bodymessage').html(pmensaje);
						$('#omessage').attr('class','message '+pcol);
					},
	modaloff	:	function() { $('#omodal').attr('class','modal'); },
	notifica	:	function(pmensaje,pcol='is-warning') { 
						$('#onotifica').css({'display':'block'}); 
						$('#onotifica').attr('class','notification '+pcol);
						$('#otexnotifica').html(pmensaje);
					},
	notificaoff	:	function() { $('#onotifica').css({'display':'none'}); },
	showPos		:	function(position) {
						fe.coordx=position.coords.latitude;
						fe.coordy=position.coords.longitude;
					}
};

//----------------------------------------------------------------------------------------------------------------------------------------------------------------------
var ini = function() {
	//fe.hostname = window.location.hostname;
	fe.origin = window.location.origin;
	//console.log(screen.width+ " x " + screen.height);
	if (navigator.geolocation) { navigator.geolocation.getCurrentPosition(fe.showPos); }
	//console.log($('#mensaje').attr('class'));
}

 //---------------------------------------------------------------------------------------------------------------------
var link = function() {
	//console.log('Ingresando a Login');
	//$("#v1").prop('disabled',true);
	//$("#vl").attr('disabled','disabled');
	//$("#v1x").prop('disabled',true);
	//$("#v1x *").attr("disabled", "disabled").off('click');
	//$('div.container').css("visibility","hidden");
	$('#formalogin').css("display","none");
	$('#formalogin2').css("display","block");
	alert('desactivando div');
}

//---------------------------------------------------------------------------------------------------------------------
var acercade = function() {
	fe.enca='</br><div align="center"><img src="http://localhost/git/swei/cf/views/images/swei_1.png" style="width: 28%;"></div></br>';
	fe.tex ='Ayudamos a las empresas a mejorar la gestión comercial y de los servicios en atención a sus clientes,</br></br>'+
			'<p style="text-align:center"><strong>...entonces estamos construyendo esto!</strong></p></br>'+
			'La plataforma para la Gestión Comercial de la industria que la organización pueda utilizar para el control de su facturación, recaudación y de los procesos comerciales del negocio en los servicios que ofrece.</br></br>'+
			'<p style="text-align:center"><strong>SAPAC-SWI versión 0.1.0-alfa</strong></p></br>'+
			'Copyright 2020 SWeI, todos los derechos reservados.</br>'+
			'El sistema SAPAC-SWI es una realidad gracias al proyecto de código fuente abierto de otros programas.</br></br>'+
			'<a>Términos y Condiciones del Servicio</a>';
	fe.modal('Acerca de...',fe.enca+fe.tex,'is-info');
}

//---------------------------------------------------------------------------------------------------------------------
var authuser = {
	mtd 	: 	'authuser_0_1',
	previo	: 	function() {  
					res=true;
					api.url=api.pathapi+this.mtd;
					fe.user=$.trim($('#fi_usuario').val());
					fe.pssw=$.trim($('#fi_password').val());
					api.post={loginuser:fe.user,loginpassword:fe.pssw,coordx:fe.coordx,coordy:fe.coordy};
					//console.log(api.post.coordx);					
					//alert(api.post); 
					$('#fih_usuario').html('');
					$('#fi_usuario').attr('class','input is-info');
					if (fe.user.length==0) { 
						$('#fih_usuario').html('ingrese su usuario');
						$('#fi_usuario').attr('class','input is-danger');
						res=false;
					} 
					$('#fih_password').html('');
					$('#fi_password').attr('class','input is-info');
					if (fe.pssw.length==0) { 
						$('#fih_password').html('ingrese su contraseña');
						$('#fi_password').attr('class','input is-danger');
						res=false;
					}	
					return res;
				},
	api		:	function (e) {
					e.preventDefault();
					if (!this.previo()) return;									
					$.post(api.url,api.post)
						.done(function(data)   { authuser.done(data); })
						.fail(function(data)   { authuser.fail(data); })
						.always(function(data) { authuser.always(data); });
				},
	done	: 	function(data) { 
					console.log(data); 
					//window.location='http://localhost/git/swei/cf/views/dashboard.php';
					window.location=data.data.dashboard;
				},
	fail	: 	function(data) { 
					fe.notifica('<center><strong>'+data.responseJSON.info+'</strong></br>(code: '+
								data.responseJSON.mtd+'_'+data.responseJSON.code+')</center>','is-info is-light'); 
				}, 
	always	: 	function(data) { null; } 
}

//---------------------------------------------------------------------------------------------------------------------
var authpro = {
	mtd 	: 	'authpro_0_1',
	previo	:	function() { 
					api.url=api.pathapi+this.mtd;
					api.post={};
				},
	api		:	function () {
					this.previo();
					$.post(api.url,api.post)
						.done(function(data)   { authpro.done(data); })
						.fail(function(data)   { authpro.fail(data); })
						.always(function(data) { authpro.always(data); });
				},
	done	: 	function(data) {
					console.log(data);
					console.log(data.data.certificado);
					//alert(data.version);
					//alert(data.timestamp);
					fe.enca='<p style="text-align:center"><strong>TEST</strong></p></br>';
					fe.ico='<span style="font-size: 24px; color: Green;"><i class="fas fa-check"></i></span>';
					fe.esp='&nbsp;&nbsp;&nbsp;&nbsp;';
					fe.modal('Acerca de...',
						fe.enca+
						fe.ico+fe.esp+'Servicio : <strong>'+data.info+'</strong></br>'+
						fe.ico+fe.esp+'Versión : <strong>'+data.version+'</strong></br>'+
						//fe.ico+fe.esp+'Certificado: '+data.data.certificado+'</br>'+
						fe.ico+fe.esp+'Razón Social : <strong>'+data.data.razon_social+'</strong></br>'+
						fe.ico+fe.esp+'Nombre Comercial : <strong>'+data.data.nombre_comercial+'</strong></br>'+
						fe.ico+fe.esp+'Número Máximo de Contratos : <strong>'+data.data.max_contratos+'</strong></br>'+
						fe.ico+fe.esp+'Vigencia del producto : <strong>'+data.data.expira_producto+'</strong></br>'+
						fe.ico+fe.esp+'Vigencia del soporte : <strong>'+data.data.expira_soporte,
						'is-info'
						);
				},
	fail	: 	function(data) { 
					//console.log(api.url); 
					//console.log(data); 
					alert(data.responseJSON.info+'  ('+this.mtd+'_'+data.responseJSON.code+')'); 
				},
	always	: 	function(data) { null; }
}

//---------------------------------------------------------------------------------------------------------------------
/*
var servicio = {
	mtd 	:	'servicio_version',
	previo	:	function() { 
 					res=true;
					api.url=api.pathapi+this.mtd;
					api.post={};
					if (<condicion para rechazar>) res=false;
					return res;
				},
	api		:	function (e) {
					e.preventDefault();
					if (!this.previo()) return;									
					$.post(api.url,api.post)
						.done(function(data)   { servicio.done(data); })
						.fail(function(data)   { servicio.fail(data); })
						.always(function(data) { servicio.always(data); });
				},
	done	: 	function(data) { null; },
	fail	: 	function(data) { null; },
	always	: 	function(data) { null; }
}
*/

//----------------------------------------------------------------------------------------------------------------------------------------------------------------------
$(document).ready(function() {
	ini(); 
	$('#lk1').click(function(e) { link(); });
	$('#bdeletemessage').click(function(e) { fe.modaloff(); });
	$('#bdeletenotifica').click(function(e) { fe.notificaoff(); });  
	$('#fb_login').click(function(e) { authuser.api(e); });  
	$('#iacercade').click(function(e) { acercade(); });
	$('#itest').click(function(e) { authpro.api(); });
});
