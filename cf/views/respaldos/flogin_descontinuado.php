<?php
/*
 * flogin.php
 * 
 * Copyright 2019 alfonsovsalinas <alfonsovsalinas@gmail.com>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */
require_once('html.php');
class Formlogin extends HtmlClass{

	public function Flogin() {
		$this->column(1,0,'is-centered','id="formalogin"');
			$this->column(1,1,'is-5');
				//$this->box();
					$this->title(null,"<h1 class='subtitle is-3 has-text-info'>Hola, gusto en verte!</h1>");
					$this->form();
						$prms=array(//"label"		=> "<label class='label'>correo</label>",
									"iconleft"	=> "fa-user",
									"input" 	=> "<input class='input is-info' type='text' id='fusuario' placeholder='ingrese su nombre de usuario' autocomplete='new-password'>",  //dígale a Chrome que esta es una nueva entrada de contraseña y no proporcionará las antiguas como sugerencias de autocompletar.
									"help" 		=> "<p class='help is-danger' id='husuario'></p>" );			
						$this->input($prms);
						$prms=array(//"label"		=> "<label class='label'>contraseña</label>",
									"iconleft"	=> "fa-lock",
									"input" 	=> "<input class='input is-info' type='password' id='fpassword' placeholder='ingrese su contraseña' autocomplete='new-password'>",  //dígale a Chrome que esta es una nueva entrada de contraseña y no proporcionará las antiguas como sugerencias de autocompletar.
									"help" 		=> "<p class='help is-danger' id='hpassword'></p>" );			
						$this->input($prms); 
						//$this->checkbox('Recuerdame');  		//$this->button('Iniciar sesión','is-info is-fullwidth','b_login');
						$this->button("<button class='button is-info is-fullwidth' id='blogin'>Iniciar sesión</button>");
						//$this->title(null,"<h7 class='subtitle is-7 has-text-dark'>¿Aún no tienes cuenta? <a href='#' id='lk1'><font color='#3399ff'>Empieza Ahora.</font></a><p></p></h7>");
						$this->title(null,"<h7 class='subtitle is-7 has-text-dark'>¿Se te olvido la contraseña? <a href='#' id='lk1'><font color='#3399ff'>Reinicia tu Contraseña.</font></a></h7>");
					$this->form(0);
				//$this->box(0);
			$this->column(0,1);
		$this->column(0);
	}
	
	public function Flogin2() {
		$this->column(1,0,"is-centered",'id="formalogin2" style="display: none;"');
		$this->column(1,1,'is-5');
		$this->box();
		$this->title(null,"<h1 class='subtitle is-3 has-text-info'>Inicia sesión2</h1>");
		$this->form();

		$prms=array(//"label"		=> "<label class='label'>correo</label>",
					"iconleft"	=> "fa-user",
					"input" 	=> "<input class='input is-info' type='text' id='fusuario2' placeholder='ingrese su nombre de usuario' autocomplete='new-password'>",  //dígale a Chrome que esta es una nueva entrada de contraseña y no proporcionará las antiguas como sugerencias de autocompletar.
					"help" 		=> "<p class='help is-danger' id='husuario'></p>" );			
		$this->input($prms);
		$prms=array(//"label"		=> "<label class='label'>contraseña</label>",
					"iconleft"	=> "fa-lock",
					"input" 	=> "<input class='input is-info' type='password' id='fpassword2' placeholder='ingrese su contraseña' autocomplete='new-password'>",  //dígale a Chrome que esta es una nueva entrada de contraseña y no proporcionará las antiguas como sugerencias de autocompletar.
					"help" 		=> "<p class='help is-danger' id='hpassword'></p>" );			
		$this->input($prms); 
		$this->checkbox('Recuerdame');  		//$this->button('Iniciar sesión','is-info is-fullwidth','b_login');
		$this->button("<button class='button is-info is-fullwidth' id='blogin2'>Iniciar sesión</button>");
		$this->title(null,"<h7 class='subtitle is-7 has-text-dark'>¿Aún no tienes cuenta? <a href='#' id='lk1'><font color='#3399ff'>Empieza Ahora.</font></a><p></p></h7>");
		$this->title(null,"<h7 class='subtitle is-7 has-text-dark'>¿Se te olvido la contraseña? <a href='#'><font color='#3399ff'>Reinicia tu Contraseña.</font></a></h7>");
		$this->form(0);
		$this->box(0);
		$this->column(0,1);
		$this->column(0);
	}


}
?>

