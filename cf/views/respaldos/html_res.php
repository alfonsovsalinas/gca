<?php
/*
 * html.php
 * 
 * Copyright 2019 alfonsovsalinas <alfonsovsalinas@gmail.com>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */

error_reporting(E_ALL|E_STRICT);

class HtmlClass {

//private $bulmadir = 'https://cdnjs.cloudflare.com/ajax/libs/bulma/0.7.5/css/bulma.min.css';
private $bulmadir	= '../../vendor/bulma/0.9.1/css/bulma.min.css';
private $jquerydir	= '../../vendor/jquery/3.4.1/jquery.min.js';
private $fontdir	= '../../vendor/fontawesome/5.12.1/css/all.css';
//private $fontdir	= 'https://use.fontawesome.com/releases/v5.3.1/js/all.js';

/* -----------------------------------------------------------------------------------------------------------------------------------------
 * Component body	-> cuerpo del html 
 * @param	-> ($ini=0,$titulo=null)
 * $ini		-> 1=abre el componente, 0=cierra el componente
 * $mdf	-> titulo de la página
 * -------------------------------------------------------------------------------------------------------------------------------------- */
public function body($ini=0,$titulo=null) {
	if ($ini==1) 
		$res= "
		<!DOCTYPE html><html>
		<head>
			<meta charset='utf-8'>
			<title>$titulo</title>
			<meta name='viewport' content='width=device-width, initial-scale=1'>
			<meta name='mobile-web-app-capable' content='yes'> 
			<link rel='icon' sizes='192x192' href='/icon.png'>
			<link rel='stylesheet' type='text/css' href='{$this->bulmadir}' />
			<link href='{$this->fontdir}' rel='stylesheet'>			
			<link rel='stylesheet' type='text/css' href='css/screen.css'/>
			<style>
				@media screen and (max-width: 768px) {
					#menu-toggle:checked + .nav-menu { display: block; }
				}
			</style>	
		</head>
		<body>
		";
	else 
		$res="</body></html>";
	echo $res;
}

/* -----------------------------------------------------------------------------------------------------------------------------------------
 * Component Columns
 * @param -> ($ini=1,$section=0,$mdfcol=null,$attsec=null)
 * $ini			-> 1=abre el componente, 0=cierra el componente
 * $section		-> 1=dentro de una sección definición columns, 0=unicamente el column
 * $mdfcol		-> propiedades del componente column
 * $attsec		-> propiedades del componente section
 * -------------------------------------------------------------------------------------------------------------------------------------- */
public function column($ini=1,$section=0,$mdfcol=null,$attsec=null) {
	if ($ini==1) 
		if ($section==1) $res="
			<section class='section' $attsec >
				<div class='columns $mdfcol'>
			";
		else
			$res="<div class='column $mdfcol'>"; 
	else 
		if ($section==1) 
			$res="</div></section>";
		else
			$res="</div>";
	echo $res;	
}

/* -----------------------------------------------------------------------------------------------------------------------------------------
 * Librerías Javascript
 * @param 	-> ($js,$uni=0)
 * $js		-> declaración de uso libreria javascript ubicado en la carpeta js 
 * $uni		-> 0: carga librerías requeridas;  1: solo una librería, la declarada en $js
 * -------------------------------------------------------------------------------------------------------------------------------------- */
public function js($js,$uni=0) {
	if $uni==0 
		$res="<script type='text/javascript' src='{$this->jquerydir}'></script>
			  <script type='text/javascript' src='js/".$js."'></script>
			 ";
	else
		$res="<script type='text/javascript' src='js/".$js."'></script>";	
	echo $res;
}

/* -----------------------------------------------------------------------------------------------------------------------------------------
 * Component Container	->	https://bulma.io/documentation/layout/container/
 * @param 	-> ($ini=0,$section=0,$mdfcon=null,$mdfsec=null)
 * $ini		-> 1=abre el componente, 0=cierra el componente
 * $section	-> 1=dentro de una sección, 0=unicamente el container
 * $mdfcon	-> propiedades del componente container
 * $mdfsec	-> propiedades del componente section
 * -------------------------------------------------------------------------------------------------------------------------------------- */
public function container($ini=0,$section=0,$mdfcon=null,$mdfsec=null) {
	if ($ini==1)
		if ($section==1) 
			$res="
				<section class='section $mdfsec'>
					<div class='container $mdfcon'>
			";
		else
			$res="<div class='container $mdfcon'>";
	else
		if ($section==1) 
			$res="</section></div>"; 
		else
			$res="</div>";
	echo $res;
}

/* -----------------------------------------------------------------------------------------------------------------------------------------
 * Component Form Input
 * @param	-> 	($prms)
 * $prms	->	arreglo de parámetros
 * 				array(
 * 				"prefijo"	=> "<div class='level-item'>",
 *				"label"		=> "<label class='label'>campo</label>",
 *				"iconleft"	=> "fa-user",
 *				"iconright"	=> "fa-check",
 *				"input" 	=> "<input class='input is-black' type='text' id='1' placeholder='escribe algo' value='' autocomplete='new-password'>",  //dígale a Chrome que esta es una nueva entrada de contraseña y no proporcionará las antiguas como sugerencias de autocompletar.
 *				"button"	=> "<button class='button is-black'>Buscar</button>",
 *				"help" 		=> "<p class='help is-black'>información de ayuda para el campo</p>" );			
 * -------------------------------------------------------------------------------------------------------------------------------------- */
public function input($prm) {
	$res=null;
	if (isset($prm['prefijo'])) $res=$prm['prefijo'];
	if (isset($prm['button'])) $res=$res."<div class='field has-addons'>"; else $res=$res."<div class='field'>";					
	$res=$res.$prm['label'];
	$res=$res."<div class='control";
	if (isset($prm['iconleft']))  $res=$res." has-icons-left";
	if (isset($prm['iconright'])) $res=$res." has-icons-right";
	$res=$res."'>";
	$res=$res.$prm['input'];
	if (isset($prm['iconleft']))  $res=$res."<span class='icon is-small is-left'><i class='fas {$prm['iconleft']}'></i></span>";
	if (isset($prm['iconright'])) $res=$res."<span class='icon is-small is-right'><i class='fas {$prm['iconright']}'></i></span>";
	$res=$res."</div>";
	if (isset($prm['button'])) $res=$res."<p class='control'>{$prm['button']}</p>";
	$res=$res.$prm['help'];
	$res=$res."</div>";
	if (isset($prm['prefijo'])) $res=$res."</div>";
	echo $res;
}

/* -----------------------------------------------------------------------------------------------------------------------------------------
 * Component Modal	->	https://bulma.io/documentation/components/modal/
 * @param 	-> ($mdfcol)
 * $mdfcon	-> modificador de color del componente
 * -------------------------------------------------------------------------------------------------------------------------------------- */
public function modal() {
	$res="
	<div class='modal' id='omodal'>
		<div class='modal-background'></div>
		<div class='modal-content'>

			<article class='message is-dark' id='omessage'>
				<div class='message-header'><p id='headmessage'>Danger</p>
					<button class='delete' aria-label='delete' id='bdeletemessage'></button>				
				</div>
				<div class='message-body'><p id='bodymessage'></p></div>
			</article>

		</div>
	</div>
	";
	echo $res;
}

/* -----------------------------------------------------------------------------------------------------------------------------------------
 * Component navbar -> https://bulma.io/documentation/components/navbar/
 * @param -> ($ini=1,$component='main',$text=null,$mdf=null,$id=null)
 * $ini			-> 1=abre el componente, 0=cierra el componente
 * $component	-> el nombre del componente
 * $text		-> texto que acompaña el componente
 * $mdf		-> propiedades del componente
 * $id			-> identificador del componente
 * -------------------------------------------------------------------------------------------------------------------------------------- */
public function navbar($ini=1,$component='main',$text=null,$mdf=null,$id=null) {
	if ($ini==1)
		switch ($component) {
			case 'main' : $res="<nav class='navbar $mdf' role='navigation' aria-label='main navigation'>"; break;
			case 'brand': $res="<div class='navbar-brand'>"; break;
			case 'img'  : $res="
				<a class='navbar-item'>
					<img src='images/".$text."'>
				</a>
				"; break;
			case 'burger': $res="
				<a role='button' class='navbar-burger burger' aria-label='menu' aria-expanded='false' data-target='navbarBasicExample'>
					<span aria-hidden='true'></span>
					<span aria-hidden='true'></span>
					<span aria-hidden='true'></span>
				</a>
				"; break;
			case 'menu' : $res="<div id='navbarBasicExample' class='navbar-menu'>"; break;
			case 'start': $res="<div class='navbar-start'>"; break;
			case 'item' : $res="<a class='navbar-item $mdf' id='$id'>$text</a>"; break;
			case 'link' : $res="
				<div class='navbar-item has-dropdown is-hoverable'>
					<a class='navbar-link'>$text</a>
					<div class='navbar-dropdown'>
				"; break;
			case 'divider' : $res="<hr class='navbar-divider'>"; break;
			case 'end'	   : $res="<div class='navbar-end'>"; break;
			case 'buttons' : $res="
				<div class='navbar-item'>
					<div class='buttons'>
				"; break;
			case 'button'  : $res="<a class='button $mdf'><strong>$text</strong></a>"; break;
			case 'tags' : $res="
				<div class='tags has-addons'>
				"; break;
			case 'tag'  : $res="<span class='tag $mdf'>$text</span>"; break;
			default: return null;
		}	
	else 
		switch ($component) {
			case 'main':  $res="</nav>"; break;
			case 'menu' :
			case 'start':
			case 'end':
			case 'brand':
			case 'tags': $res="</div>"; break;
			case 'buttons':
			case 'link' : $res="</div></div>"; break;
			default: return null;
		}	
	echo $res;			
}

/* -----------------------------------------------------------------------------------------------------------------------------------------
 * Component Notification	->	https://bulma.io/documentation/elements/notification/
 * @param 	-> ($mdfcol)
 * $mdfcon	-> modificador de color del componente
 * -------------------------------------------------------------------------------------------------------------------------------------- */
public function notifica($mdfcol='is-info is-light') {
	$res="
	<div class='notification $mdfcol' id='onotifica' style='display: none;'>
		<button class='delete' id='bdeletenotifica'></button><p id='otexnotifica'></p>
	</div>
	";
	echo $res;
}

/* -----------------------------------------------------------------------------------------------------------------------------------------
 * Component Subtitle	->	https://bulma.io/documentation/elements/title/
 * @param 	-> ($tex,$mdf=null)
 * $tex		-> el texto 
 * $mdf		-> el modificador subtitle
 * -------------------------------------------------------------------------------------------------------------------------------------- */
public function subtitle($tex,$mdf=null) {
	$res="<p class='subtitle $mdf'>$tex</p>";
	echo $res;
}

/* -----------------------------------------------------------------------------------------------------------------------------------------
 * Component Subtitle	->	https://bulma.io/documentation/elements/title/
 * @param 	-> ($tex,$mdf=null)
 * $tex		-> el texto 
 * $mdf		-> el modificador title
 * -------------------------------------------------------------------------------------------------------------------------------------- */
public function title($tex,$mdf=null) {
	$res="<p class='title $mdf'>$tex</p>";
	echo $res;
}


//======================================================================================================================================================================	
//======================================================================================================================================================================	


 
//----------------------------------------------------------------------------------------------------------------------------------------------------------------------	
//Form   $attr=onsubmit="return vlogin()
//----------------------------------------------------------------------------------------------------------------------------------------------------------------------	
public function form($ini=1,$attr=null) {
	if ($ini==1) $res="<form $attr>";	else $res="</form>";
	echo $res;	
}
  
//----------------------------------------------------------------------------------------------------------------------------------------------------------------------	
//Layout herobody
//----------------------------------------------------------------------------------------------------------------------------------------------------------------------	
public function herobody($ini=1,$attr=null) {
	if ($ini==1) 
		$res="
		<section class='hero $attr'>
			<div class='hero-body'>
				<div class='container'>
		"; 
	else 
		$res="</div></div></section>";
	echo $res;
}

//----------------------------------------------------------------------------------------------------------------------------------------------------------------------	
// Layout Level
//----------------------------------------------------------------------------------------------------------------------------------------------------------------------	
public function level($ini=1,$comp='level') {
	if ($ini==1)
		switch ($comp) {
			case 'level' : $res="<nav class='level'>"; break;
			case 'left'  : 
			case 'right' : $res="<div class='level-$comp'>"; break;
			default: return null;
		}	
	else 
		if ($comp=='level') $res="</nav"; else $res="</div>"; 
	echo $res;			
}

//----------------------------------------------------------------------------------------------------------------------------------------------------------------------	
//Form Checkbox
//----------------------------------------------------------------------------------------------------------------------------------------------------------------------	
public function checkbox($tex) {
	$res="
		<div class='field'>
			<div class='control'>
				<label class='checkbox'>
					<input type='checkbox'>
					$tex
				</label>
			</div>
		</div>
		";
	echo $res;
}
	
	
///----------------------------------------------------------------------------------------------------------------------------------------------------------------------	
//Form Input
//$prms=array("prefijo"		=> "<div class='level-item'>",	<1
			//"type"		=> "text", 			x
			//"id"			=> "id", 			x
			//"label"		=> "campo", 			<2
			//"placeholder"	=> "escriba algo", 	x
			//"autocomplete"=> "off",			x
			//"value"		=> "valor inicio", 	x
			//"iconleft"	=> "fa-user", 		<3
			//"iconright"	=> "fa-check", 		<4
			//class='input is-black' type='text' id='1' placeholder='escribe algo' value='' autocomplete='new-password'  //dígale a Chrome que esta es una nueva entrada de contraseña y no proporcionará las antiguas como sugerencias de autocompletar.
			//class='help is-black'
			//"help"		=> "información de ayuda para el campo", <
			//"col"			=> "is-black", 		x
			//"colhelp"		=> "is-black", 		x
			//"textbutton"	=> "Buscar" );		<
//----------------------------------------------------------------------------------------------------------------------------------------------------------------------	
//public function input($prm) {
	//$res=null;
	//if (isset($prm['prefijo'])) $res=$prm['prefijo'];
	//if (isset($prm['textbutton'])) $res=$res."<div class='field has-addons'>"; else $res=$res."<div class='field'>";
					
	//if (isset($prm['label'])) $res=$res."<label class='label'>{$prm['label']}</label>";
	//$res=$res."<div class='control";
	//if (isset($prm['iconleft']))  $res=$res." has-icons-left";
	//if (isset($prm['iconright'])) $res=$res." has-icons-right";
	//$res=$res."'>";
	//$res=$res."<input class='input";
	//if (isset($prm['col'])) $res=$res." {$prm['col']}";
	//$res=$res."' type='{$prm['type']}'";
	//if (isset($prm['id'])) $res=$res." id='{$prm['id']}'";
	//if (isset($prm['placeholder']))  $res=$res." placeholder='{$prm['placeholder']}'";
	//if (isset($prm['value']))  $res=$res." value='{$prm['value']}'";
	//if (isset($prm['autocomplete'])) 
	//if ($prm['autocomplete']=='off') $res=$res." autocomplete='new-password'";   //dígale a Chrome que esta es una nueva entrada de contraseña y no proporcionará las antiguas como sugerencias de autocompletar.
	//$res=$res.">";
	//if (isset($prm['iconleft']))  $res=$res."<span class='icon is-small is-left'><i class='fas {$prm['iconleft']}'></i></span>";
	//if (isset($prm['iconright'])) $res=$res."<span class='icon is-small is-right'><i class='fas {$prm['iconright']}'></i></span>";
	//$res=$res."</div>";
	//if (isset($prm['textbutton'])) $res=$res."<p class='control'><button class='button {$prm['col']}'>{$prm['textbutton']}</button></p>";
	//$res=$res."<p class='help {$prm['colhelp']}'>{$prm['help']}</p>";
	//$res=$res."</div>";
	//if (isset($prm['prefijo'])) $res=$res."</div>";
	//echo $res;
//}


		
//----------------------------------------------------------------------------------------------------------------------------------------------------------------------	
//Form Select
//----------------------------------------------------------------------------------------------------------------------------------------------------------------------	
public function select($prefijo=null,$opc=[]) {
	$res=null;
	if (!empty($prefijo)) $res=$prefijo;
	$res=$res."<div class='select'><select>";
	for ($i=0; $i<count($opc); $i++) $res=$res."<option>$opc[$i]</option>";
	$res=$res."</select></div>";
	if (!empty($prefijo)) $res=$res."</div>";
	echo $res;
}
	
//----------------------------------------------------------------------------------------------------------------------------------------------------------------------	
// Element Box
//----------------------------------------------------------------------------------------------------------------------------------------------------------------------	
public function box($ini=1) {
	if ($ini==1) $res="<div class='box'>"; else $res="</div>";
	//if ($ini==1) $res="<div class='notification is-warning'>login"; else $res="</div>";
	echo $res;
}

/* -----------------------------------------------------------------------------------------------------------------------------------------
 * Component Button
 * @param -> ($button)
 * $button	-> toda la descripción de la clase button.  Ej.<button class='button is-info is-fullwidth' id='blogin'>Iniciar sesión</button>
 * -------------------------------------------------------------------------------------------------------------------------------------- */
public function button($button) {
	$res="
		<div class='field'>
			<div class='control'>
				$button
			</div>
		</div>
		"; 
	echo $res;
}

/* -----------------------------------------------------------------------------------------------------------------------------------------
 * Component Title	->	https://bulma.io/documentation/elements/title/
 * @param 	-> ($prefijo=null,$tex)
 * $prefijo	-> cuando es parte de un componente
 * $tex		-> el texto del titulo
  * Component Title	->	https://bulma.io/documentation/elements/title/
 * @param 	-> ($n=3,$tex,$clase='subtitle',$mdf=null)
 * $n		->	el numero del tag h
 * $clase	-> 	titlcuando es parte de un componente
 * $tex		-> el texto del titulo
* -------------------------------------------------------------------------------------------------------------------------------------- */
//public function title($n=3,$tex,$mdf=null) {
	//res="<h$n class='title $mdf'>$tex</h$n>"
	//echo $res;
//}

//public function title($prefijo=null,$tex) {
	//$res=null;
	//if (!empty($prefijo)) $res=$prefijo;
	//$res=$res.$tex;
	//if (!empty($prefijo)) $res=$res."</div>";
	//echo $res;
//}

//----------------------------------------------------------------------------------------------------------------------------------------------------------------------	
// Element modal
//----------------------------------------------------------------------------------------------------------------------------------------------------------------------	
		//<button class='modal-close is-large' aria-label='close'></button>
//<div class='notification is-info' id='notimodal'>
	//<button class='delete' id='bcierramsg'></button>
	//<p id='msgmodal'></p>
//</div>
				//<div class='message-header' id='msgheadmodal'><p></p>
					//<button class='delete' aria-label='delete'></button>				
				//</div>
				
//<div class="modal">
	//<div class="modal-background"></div>
	//<div class="modal-content">
		//<!-- Any other Bulma elements you want -->
	//</div>
	//<button class="modal-close is-large" aria-label="close"></button>
//</div>



//----------------------------------------------------------------------------------------------------------------------------------------------------------------------	
// Component Menu
//----------------------------------------------------------------------------------------------------------------------------------------------------------------------	
public function menu($ini=1,$comp='main',$val=[],$possl=0,$valsl=[]) {
	if ($ini==1)
		switch ($comp) {
			case 'main'  : $res="<aside class='menu'>"; break;
			case 'list'  : 
				$res=null;
				$n=count($val);
				if ($n>0) $res="<p class='menu-label'>$val[0]</p>";
				$res=$res."<ul class='menu-list'>";
				for ($i=0; $i<$n; $i++) {
					if ($i==0) continue;
					if ($i==$possl) {
						$res=$res."<li><a class='is-active'>$valsl[0]</a><ul>";
						for ($j=0; $j<count($valsl); $j++) {
							$res=$res."<li><a>$valsl[$j]</a></li>";
						}
						$res=$res."</ul></li>";
					} 
					$res=$res."<li><a>$val[$i]</a></li>";
				}
				$res=$res."</ul>";	
				break;			
			default: return null;
		}	
	else 
		$res="</aside>"; 
	echo $res;	
}
/*
 * 
 * name: desconocido
 * 
 * @return
 * 
 */
	

//----------------------------------------------------------------------------------------------------------------------------------------------------------------------	
}
?>
