<?php
/*
 * objfe.php -> objetos del front end  
 * 
 * Copyright 2019 alfonsovsalinas <alfonsovsalinas@gmail.com>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */
 
require_once('html.php');
class FEClass extends HtmlClass{

	public function barranavegacion() {
		$this->navbar(1,'main',null,'is-info');  //is-bold
			$this->navbar(1,'brand');
				$this->navbar(1,'img','agk-bco.png');
				//$this->navbar(1,'img','caoba-cyn.png');
				//$this->navbar(1,'img','sapac_2.png');
				$this->navbar(1,'burger');
			$this->navbar(0,'brand');
			$this->navbar(1,'menu');
				$this->navbar(1,'start');
					//$this->navbar(1,'item','<span class="icon"><i class="fas fa-home fa-lg"></i></span><p>Inicio</p>');		
					//$this->navbar(1,'item','<span class="icon"><i class="fas fa-question fa-lg"></i></span><p>Ayuda</p>');		
					//$this->navbar(1,'item','<span class="icon"><i class="fas fa-comment fa-lg"></i></span><p>Blog</p>');		
				$this->navbar(0,'start');

				$this->navbar(1,'end');
					$this->navbar(1,'item','<span class="icon has-text-success"><i class="fas fa-bell fa-lg"></i></span><p>0</p>');
					$this->navbar(1,'link','<span class="icon"><i class="fas fa-user fa-lg"></i></span>');
						$this->navbar(1,'item','Mi Perfil');
						$this->navbar(1,'item','Recuperar Contraseña');
						$this->navbar(1,'item','Configuraciones');
					$this->navbar(0,'link');
					$this->navbar(1,'link','<span class="icon"><i class="fas fa-question fa-lg"></i></span>');		
						$this->navbar(1,'item','Reporte un problema');
						$this->navbar(1,'item','Logs');
						$this->navbar(1,'divider');
						$this->navbar(1,'item','Test',null,'itest');
						$this->navbar(1,'item','Contacto');
						$this->navbar(1,'item','Acerca de...',null,'iacercade');
					$this->navbar(0,'link');
					$this->navbar(1,'tags');
						$this->navbar(1,'tag','CF','is-info is-rounded');
						$this->navbar(1,'tag','0.1.0-alfa','is-light is-rounded');
					$this->navbar(0,'tags');
					//$this->navbar(1,'buttons');
						//$this->navbar(1,'button','Inicia sesión','is-dark');
					//$this->navbar(0,'buttons');
				$this->navbar(0,'end');				
			$this->navbar(0,'menu');
		$this->navbar(0,'main');
	}

	public function Flogin() {
		$this->column(1,1,'is-centered','id="f_login"');
			$this->column(1,0,'is-5');
				$this->subtitle('Hola, gusto en verte!','is-3 has-text-info');
				$prms=array(//"label"		=> "<label class='label'>correo</label>",
							"iconleft"	=> "fa-user",
							"input" 	=> "<input class='input is-info' type='text' id='fi_usuario' placeholder='ingrese su nombre de usuario' autocomplete='new-password'>",  //dígale a Chrome que esta es una nueva entrada de contraseña y no proporcionará las antiguas como sugerencias de autocompletar.
							"help" 		=> "<p class='help is-danger' id='fih_usuario'></p>" );			
				$this->input($prms);
				$prms=array(//"label"		=> "<label class='label'>contraseña</label>",
							"iconleft"	=> "fa-lock",
							"input" 	=> "<input class='input is-info' type='password' id='fi_password' placeholder='ingrese su contraseña' autocomplete='new-password'>",  //dígale a Chrome que esta es una nueva entrada de contraseña y no proporcionará las antiguas como sugerencias de autocompletar.
							"help" 		=> "<p class='help is-danger' id='fih_password'></p>" );			
				$this->input($prms); 
				$this->button("<button class='button is-info is-fullwidth' id='fb_login'>Iniciar sesión</button>");
				$this->subtitle("¿Se te olvido la contraseña? <a href='#' id='lk1'><font color='#3399ff'>Reinicia tu Contraseña.</font></a>","is-7 has-text-dark");
			$this->column(0,0);
		$this->column(0,1);
	}


	public function Flogin2() {
		$this->column(1,1,'is-centered','id="formalogin"');
			$this->column(1,0,'is-5');
				//$this->box();
					$this->title(null,"<h1 class='subtitle is-3 has-text-info'>Hola, gusto en verte!</h1>");
					$this->form();
						$prms=array(//"label"		=> "<label class='label'>correo</label>",
									"iconleft"	=> "fa-user",
									"input" 	=> "<input class='input is-info' type='text' id='fusuario' placeholder='ingrese su nombre de usuario' autocomplete='new-password'>",  //dígale a Chrome que esta es una nueva entrada de contraseña y no proporcionará las antiguas como sugerencias de autocompletar.
									"help" 		=> "<p class='help is-danger' id='husuario'></p>" );			
						$this->input($prms);
						$prms=array(//"label"		=> "<label class='label'>contraseña</label>",
									"iconleft"	=> "fa-lock",
									"input" 	=> "<input class='input is-info' type='password' id='fpassword' placeholder='ingrese su contraseña' autocomplete='new-password'>",  //dígale a Chrome que esta es una nueva entrada de contraseña y no proporcionará las antiguas como sugerencias de autocompletar.
									"help" 		=> "<p class='help is-danger' id='hpassword'></p>" );			
						$this->input($prms); 
						//$this->checkbox('Recuerdame');  		//$this->button('Iniciar sesión','is-info is-fullwidth','b_login');
						$this->button("<button class='button is-info is-fullwidth' id='blogin'>Iniciar sesión</button>");
						//$this->title(null,"<h7 class='subtitle is-7 has-text-dark'>¿Aún no tienes cuenta? <a href='#' id='lk1'><font color='#3399ff'>Empieza Ahora.</font></a><p></p></h7>");
						$this->title(null,"<h7 class='subtitle is-7 has-text-dark'>¿Se te olvido la contraseña? <a href='#' id='lk1'><font color='#3399ff'>Reinicia tu Contraseña.</font></a></h7>");
						//$this->subtitle(7,"¿Se te olvido la contraseña? <a href='#' id='lk1'><font color='#3399ff'>Reinicia tu Contraseña.</font></a>","is-7 has-text-dark");
					$this->form(0);
				//$this->box(0);
			$this->column(0,0);
		$this->column(0,1);
	}


	public function menuizquierdo() {
		$op1=array('General','Dashboard','Costumers');
		$op2=array('Administration','Team Settings','Invitations','Cloud Storage Environment Settings','Authentication');
		$subop=array('Manage your team','Members','Plugins','Add a memeber');		
		$this->menu(1);
		$this->menu(1,'list',$op1);
		$this->menu(1,'list',$op2,2,$subop);
		$this->menu(0);
	}
	
	public function toolbar() {
		$this->level(1);
		$this->level(1,'left');
			$this->subtitle("<div class='level-item'>","<strong>123</strong> posts",5);
			$prms=array ("prefijo"=>"<div class='level-item'>", "type"=>"text", "placeholder"=>"escriba algo", "iconleft"=>"fa-search", "iconright"=>"fa-check", "col"=>"is-black", "textbutton"=>"Buscar");	
			$this->input($prms);
		$this->level(0,'left');
		
		$this->level(1,'right');
			$this->subtitle("<div class='level-item'>","Order by",6);
			$op=array('Publish','Price','Page count');
			$this->select("<div class='level-item'>",$op);
		$this->level(0,'right');
		$this->level(0);
	} 
}	
?>
