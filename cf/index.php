<?php
	//cambio de config. del php.ini
	ini_set("session.use_only_cookies","1"); 
	ini_set("session.use_trans_sid","0"); 
    //cambiamos la duración a la cookie de la sesión 	
	//session_set_cookie_params(0, "/", $HTTP_SERVER_VARS["HTTP_HOST"], 0); 
	session_start();
	if (!isset($_SESSION['sesionusr'])){ 
		$_SESSION['sesionusr']=false;
		$_SESSION['auth']='0';
		header('Location: views/login.php');		
	} else {
		if ($_SESSION['sesionusr']) 
			header ('Location: views/dbfyc.php');
		else
			header ('Location: views/login.php');		
	}
	session_unset();
	session_destroy();
	exit();
?>
