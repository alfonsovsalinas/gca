<?php
	ini_set("session.use_only_cookies","1");
	ini_set("session.use_trans_sid","0");
	header('Content-Type: application/json');
	session_start();
	
	require_once('rest.php');

	class Api extends Rest {
		//--------------------------------------------------------------------------------------------------------------
		//private $_cnxdata = array('usr'=>'app','psw'=>'app','host'=>'pbayax03.aguakan.mx:1522/x7agk');
		private $_cnxdata = array('usr'=>'app','psw'=>'app','host'=>'x7rptyax01.aguakan.mx:1522/x7agk');
		//private $_cnxdata = array('usr'=>'app','psw'=>'app2017','host'=>'koon.aguakan.mx:1521/x7agk');
		//--------------------------------------------------------------------------------------------------------------
		private $_cnxdatamy = array('user'=>'api','pssw'=>'Miyo.1405','host'=>'localhost','dbname'=>'','port'=>3306,'socket'=>'' );
		//--------------------------------------------------------------------------------------------------------------
		//private $_cnxdatapg = 'host=172.30.30.15 dbname=postgres user=postgres password=4gu4k4n#20 port=5432';
		private $_cnxdatapg = 'host=localhost dbname=postgres user=postgres password=miapao port=5433';
		//--------------------------------------------------------------------------------------------------------------
		private $_mtdauth = array('logincnt'=>'pbayax03.aguakan.mx:1522/x7agk',  'loginrnx'=>'pbayax03.aguakan.mx:1522/x7agk');
		//private $_mtdauth = array('logincnt'=>'x7rptyax01.aguakan.mx:1522/x7agk','loginrnx'=>'x7rptyax01.aguakan.mx:1522/x7agk');
		//private $_mtdauth = array('logincnt'=>'koon.aguakan.mx:1521/x7agk',  'loginrnx'=>'koon.aguakan.mx:1521/x7agk');
		//--------------------------------------------------------------------------------------------------------------
		private $_conn = null;

		public function __construct() {
			parent::__construct();
		}

		public function conectarDB() {
			$this->_conn = oci_connect($this->_cnxdata['usr'],$this->_cnxdata['psw'],$this->_cnxdata['host']);
			if (!$this->_conn) return false; else return true;
		}

		public function conectarDBpg() {
			$this->_conn = pg_connect($this->_cnxdatapg); // or die('No se ha podido conectar: ' . pg_last_error());
			if (!$this->_conn) return false; else return true;
		}

		public function conectarDBmy() {
			$this->_conn = new mysqli($this->_cnxdatamy['host'],$this->_cnxdatamy['user'],$this->_cnxdatamy['pssw'],$this->_cnxdatamy['dbname'],$this->_cnxdatamy['port'],$this->_cnxdatamy['socket'])
				or die ('Could not connect to the database server' . mysqli_connect_error());			
			if (!$this->_conn) return false; else return true;
		}

		private function devolverError($id) {
			$errores = array(
				array('code' => 'API00', 'asunto' => 'petición sin contenido', 'mensaje' => 'La solicitud no tiene los elementos para su demanda.'),
				array('code' => 'API01', 'asunto' => 'petición de método no aceptada', 'mensaje' => 'Petición incorrecta para la demanda del servicio.'),
				array('code' => 'API02', 'asunto' => 'El servicio no se encuentra disponible en este momento', 'mensaje' => 'No se puede acceder al servicio.'),
				array('code' => 'API03', 'asunto' => 'Servicio no autorizado, ha expirado su acceso', 'mensaje' => 'Ha expirado la fecha de uso del token.'),
				array('code' => 'API04', 'asunto' => 'El token del servicio es inválido', 'mensaje' => 'No cumple el formato para ser reconocido.'),
				array('code' => 'API05', 'asunto' => 'La autenticación del usuario es inválido', 'mensaje' => 'No se pudo autenticar la demanda de los servicios a la Base de Datos.'),
				array('code' => 'API06', 'asunto' => 'La API no tiene conexión a la Base de Datos', 'mensaje' => 'Sin respuesta.'),
				array('code' => 'API07', 'asunto' => 'Usuario y/o password incorrecto', 'mensaje' => 'Acceso inválido.'),
				);
			return $errores[$id];
		}

		private function getIP()
		{
			if (isset($_SERVER["HTTP_CLIENT_IP"]))	{ return $_SERVER["HTTP_CLIENT_IP"]; }
			elseif (isset($_SERVER["HTTP_X_FORWARDED_FOR"]))	{ return $_SERVER["HTTP_X_FORWARDED_FOR"]; }
			elseif (isset($_SERVER["HTTP_X_FORWARDED"]))		{ return $_SERVER["HTTP_X_FORWARDED"]; }
			elseif (isset($_SERVER["HTTP_FORWARDED_FOR"]))		{ return $_SERVER["HTTP_FORWARDED_FOR"]; }
			elseif (isset($_SERVER["HTTP_FORWARDED"]))			{ return $_SERVER["HTTP_FORWARDED"]; }
			else { return $_SERVER["REMOTE_ADDR"]; }
		}
		
		private function get_public_ip(){
			$externalContent = file_get_contents('http://checkip.dyndns.com/');
			preg_match('/Current IP Address: \[?([:.0-9a-fA-F]+)\]?/', $externalContent, $m);
			$externalIp = $m[1];
			return $externalIp;
		}
	
		public function procesarLlamada() {			
			if (!isset($_REQUEST['urlget']))	//identifica la existencia de la variable urlget
				$this->mostrarRespuesta(json_encode($this->devolverError(0)), 400);
			//si por ejemplo pasamos explode('/','////controller///method////args///') el resultado es un array con elem vacios;
			//Array ( [0] => [1] => [2] => [3] => [4] => controller [5] => [6] => [7] => method [8] => [9] => [10] => [11] => args [12] => [13] => [14] => )
			$url = trim($_REQUEST['urlget']);
			if (substr($url,0,1)=='/') $url = substr($url,1);
			if (substr($url,strlen($url)-1)=='/') $url=substr($url,0,strlen($url)-1);
			//se establece un arreglo con el contenido de url
			$url = explode('/',$url);  
			//con array_filter() filtramos elementos de un array pasando función callback, que es opcional.
			//si no le pasamos función callback, los elementos false o vacios del array serán borrados
			//por lo tanto la entre la anterior función (explode) y esta eliminammethod_existsiapos los '/' sobrantes de la URL
			$url = array_filter($url);		//un arreglo de indice numérico
			$api = strtolower(array_shift($url));
			$ver = strtolower(array_shift($url));
			$argumentos = $url;
			//echo "url->".print_r($url,true)."\n";
						
			if (!isset($_SESSION['auth'])) $_SESSION['auth'] = null;
					
			$ip = $this->getIP();
			echo print_r($ip,true)."\n";
			//$theExternalIP = $this->get_public_ip();
			//echo print_r($theExternalIP,true)."\n";
			//if (strlen($ip)<6) $ip=$theExternalIP;
			//echo print_r($this->datosPeticion,true)."\n";	
			//echo print_r(array_shift($this->datosPeticion),true)."\n";	
		}

	}

	$api = new Api();
	$api->procesarLlamada();
?>
