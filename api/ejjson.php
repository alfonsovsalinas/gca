SOLICITUD EJEMPLO PARAMETROS - ITEMS

{
	"parámetro":"value",
	"parámetro con propiedades": { "propiedad": "value", ...},
	...
}

ejemplo:

{
	"account": {
		"number": "012180015001324497",
		"numberType": "CLABE"
	},
	"card": {
		"number": "5174390000066724"
	},
	"otp": "23364682"
}

RESULTADO OK SIN DATA
{	
	"code":"0",
	"info":"información del resultado"
}

ejemplo:
{
	"code":"0",
	"info": "The card was vinculated successfully."
}

RESULTADO OK CON DATA
{	
	"code":"0",
	"info":"información del resultado",
	"data": {
		"item":"valor",
		"objeto": {item:"valor",...},
		...
		}
}
ejemplo:
{
	"code":"0",
	"info": "La cuenta ha sido creada, en breve recibirás un correo con los datos de la misma.",
	"data": {
		"account ": {
			"numberFormats ": {
				"number": "012180015001324497",
				"numberType": "CLABE"
			}
		}
	}
}
RESULTADO OK CON LISTA DE DATA
{	
	"code":"0",
	"info":"información del resultado",
	"data": [{
		"item":"valor",
		"objeto": {item:"valor",...},
		...
		},
		{...},
		...
		]
}

RESULTADO CON ERROR
{
	"code": "CODE",
    "http_code": HTTP_CODE,
    "info": "error information"
}

{
    "code": "UNDER_MAINTENANCE",
    "http_code": 503,
    "info": "This app is currently undergoing maintenance. Retry in few minutes."
}

-------------------------------------------------------------------------------------------------------------------------------------------
ANOTACIONES DE ANÁLISIS...  
-------------------------------------------------------------------------------------------------------------------------------------------

{
"collection" :
	{
	"version" : "1.0",
	"href" : "http://localhost:1337/api/",
	"items" :
		[{
		"href": "http://localhost:1337/api/2csl73jr6j5",
		"data": [
			{
			"name": "text",
			"value": "Bird"
			},
			{
			"name": "date_posted",
			"value": "2013-01-24T18:40:42.190Z"
			}
			]
		}],
	"template" : {
		"data" : [
			{"prompt" : "Text of message", "name" : "text", "value" : ""}
		]
	}
}
}
-------------------------------------------------------------------------------------------------------------------------------------------
BBVA EXAMPLE
{

	"result": {
		"code": 0,
		"info": "La cuenta ha sido creada, en breve recibirás un correo con los datos de la misma."
	},
	"data": {
		"account ": {
			"numberFormats ": [{
				"number": "012180015001324497",
				"numberType": "CLABE"
			}]
		}
	}
}

REQUEST
{
	"account": {
		"number": "012180015001324497",
		"numberType": "CLABE"
	},
	"card": {
		"number": "5174390000066724"
	},
	"otp": "23364682"
}

RESPONSE
{
	"result": {
		"code": 0,
		"info": "The card was vinculated successfully."
	}
}
--------------------------------------------------------------------------------------------------------------------------------------------

{
"version" : "1.0",
"object" : "list",
"url" : "http://localhost:1337/api/",
"data" : [
	{"prompt" : "Text of message", "name" : "text", "value" : ""}
]
}

{
    "error": {
        "code": "CODE",
        "http_code": HTTP_CODE,
        "message": "error information"
    }
}

{
    "error": {
        "code": "UNDER_MAINTENANCE",
        "http_code": 503,
        "message": "This app is currently undergoing maintenance. Retry in few minutes."
    }
}
