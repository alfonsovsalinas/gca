<?php
	ini_set("session.use_only_cookies","1");
	ini_set("session.use_trans_sid","0");
	header('Content-Type: application/json');
	session_start();
	
	require_once('rest.php');

	class Api extends Rest {
		//--------------------------------------------------------------------------------------------------------------
		//private $_cnxdata = array('usr'=>'app','psw'=>'app','host'=>'pbayax03.aguakan.mx:1522/x7agk');
		private $_cnxdata = array('usr'=>'app','psw'=>'app','host'=>'x7rptyax01.aguakan.mx:1522/x7agk');
		//private $_cnxdata = array('usr'=>'app','psw'=>'app2017','host'=>'koon.aguakan.mx:1521/x7agk');
		//--------------------------------------------------------------------------------------------------------------
		private $_cnxdatamy = array('user'=>'api','pssw'=>'Miyo.1405','host'=>'localhost','dbname'=>'','port'=>3306,'socket'=>'' );
		//--------------------------------------------------------------------------------------------------------------
		//private $_cnxdatapg = 'host=172.30.30.15 dbname=postgres user=postgres password=4gu4k4n#20 port=5432';
		private $_cnxdatapg = 'host=localhost dbname=postgres user=postgres password=miapao port=5433';
		//--------------------------------------------------------------------------------------------------------------
		private $_conn = null;

		public function __construct() {
			parent::__construct();
		}

		public function conectarDB() {
			$this->_conn = oci_connect($this->_cnxdata['usr'],$this->_cnxdata['psw'],$this->_cnxdata['host']);
			if (!$this->_conn) return false; else return true;
		}

		public function conectarDBpg() {
			$this->_conn = pg_connect($this->_cnxdatapg); // or die('No se ha podido conectar: ' . pg_last_error());
			if (!$this->_conn) return false; else return true;
		}

		public function conectarDBmy() {
			$this->_conn = new mysqli($this->_cnxdatamy['host'],$this->_cnxdatamy['user'],$this->_cnxdatamy['pssw'],$this->_cnxdatamy['dbname'],$this->_cnxdatamy['port'],$this->_cnxdatamy['socket'])
				or die ('Could not connect to the database server' . mysqli_connect_error());			
			if (!$this->_conn) return false; else return true;
		}

		private function devolverError($id) {
			$errores = array(
				array('code' => 'API_INCOMPLETO','http_code' => 400, 'info' => 'Solicitud de servicio incompleta, no se tienen los elementos necesarios para su demanda.'),
				array('code' => 'API_NOCNXDB',	 'http_code' => 500, 'info' => 'Sin conexión a la Base de Datos, no responde.'),
				array('code' => 'API_SRVNOEXS',	 'http_code' => 404, 'info' => 'El servicio no existe o ha expirado su acceso.'),
				array('code' => 'API_SRVNODIS',  'http_code' => 403, 'info' => 'El servicio no se encuentra disponible en este momento, no responde.'),
				);
			return $errores[$id];
		}

		private function getIP()
		{
			if (isset($_SERVER["HTTP_CLIENT_IP"]))	{ return $_SERVER["HTTP_CLIENT_IP"]; }
			elseif (isset($_SERVER["HTTP_X_FORWARDED_FOR"]))	{ return $_SERVER["HTTP_X_FORWARDED_FOR"]; }
			elseif (isset($_SERVER["HTTP_X_FORWARDED"]))		{ return $_SERVER["HTTP_X_FORWARDED"]; }
			elseif (isset($_SERVER["HTTP_FORWARDED_FOR"]))		{ return $_SERVER["HTTP_FORWARDED_FOR"]; }
			elseif (isset($_SERVER["HTTP_FORWARDED"]))			{ return $_SERVER["HTTP_FORWARDED"]; }
			else { return $_SERVER["REMOTE_ADDR"]; }
		}
		
		private function get_public_ip(){
			$externalContent = file_get_contents('http://checkip.dyndns.com/');
			preg_match('/Current IP Address: \[?([:.0-9a-fA-F]+)\]?/', $externalContent, $m);
			$externalIp = $m[1];
			return $externalIp;
		}
	
		public function procesarLlamada() {			
			if (!isset($_REQUEST['urlget']))	//identifica la existencia de la variable urlget
				$this->mostrarRespuesta(json_encode($this->devolverError(0)), 400);
			//si por ejemplo pasamos explode('/','////controller///method////args///') el resultado es un array con elem vacios;
			//Array ( [0] => [1] => [2] => [3] => [4] => controller [5] => [6] => [7] => method [8] => [9] => [10] => [11] => args [12] => [13] => [14] => )
			$url = trim($_REQUEST['urlget']);
			if (substr($url,0,1)=='/') $url = substr($url,1);
			if (substr($url,strlen($url)-1)=='/') $url=substr($url,0,strlen($url)-1);
			//se establece un arreglo con el contenido de url
			$url = explode('/',$url);  
			//con array_filter() filtramos elementos de un array pasando función callback, que es opcional.
			//si no le pasamos función callback, los elementos false o vacios del array serán borrados
			//por lo tanto la entre la anterior función (explode) y esta eliminammethod_existsiapos los '/' sobrantes de la URL
			$url = array_filter($url);		//un arreglo de indice numérico
			$mtd = strtolower(array_shift($url));
			$argumentos = $url;
			if (!isset($_SESSION['cerpro'])) $_SESSION['cerpro'] = $this->certificado['certificado'];	// la autenticación del producto
			if (!isset($_SESSION['visita'])) $_SESSION['visita'] = 0;
			if (substr(strtoupper($mtd),0,7)=='AUTHPRO' and $_SESSION['visita']==0) $_SESSION['visita'] = 1; // se le da permiso a Authpro
			//echo 'certificado -> '.print_r($this->certificado['certificado'],true)."\n";
			//echo 'cerpro -> '.print_r($_SESSION['cerpro'],true)."\n";
			$cambiocnx=false;
			if (array_key_exists('loginuser',$this->datosPeticion)) {
				if (!array_key_exists('loginpassword',$this->datosPeticion) or
					!array_key_exists('coordx',$this->datosPeticion) or
					!array_key_exists('coordy',$this->datosPeticion))	
					$this->mostrarRespuesta(json_encode($this->devolverError(0)), 400);
				$ip=null;
				$_SESSION['visita'] = 0;
				//$ip = $this->getIP();
				//if (strlen($ip)<6) {
					//$theExternalIP = $this->get_public_ip();
					//$realIP = file_get_contents("http://ipecho.net/plain");
					//echo 'realIP -> '.print_r($realIP ,true)."\n";
					//$ip=$theExternalIP;
				//}
				$this->cadena=$this->datosPeticion['loginpassword'];
				//echo 'password -> '.print_r($this->datosPeticion['loginpassword'],true)."\n";
				//echo 'md5 -> '.print_r($this->keymd5(),true)."\n";
				$this->_postdata = array('mtd' 		=> $mtd,
										 'usuario'	=> $this->datosPeticion['loginuser'],
										 'password'	=> $this->keymd5(),
										 'ip'		=> $ip,
										 'coorx'	=> $this->datosPeticion['coordx'],
										 'coory'	=> $this->datosPeticion['coordy']
										 );
				$cambiocnx=true;
				$param='';
				foreach ($this->_postdata as $key => $v) { 
					$this->_postdata[$key]=htmlspecialchars_decode($v);
					if ($key =='mtd' or $key=='ver') continue;
					if (empty($param)) $param="'".$v."'"; else $param=$param.",'".$v."'"; 
				}
			} else {
				$this->_postdata = array('mtd' => $mtd, 'ver' => $ver);	// 'auth' => $_SESSION['auth']
			
				$i = 0; $param=''; 
				foreach ($argumentos as $v) { 
					$i++; $this->_postdata['prm'.$i]=$v; 
					if (empty($param)) $param="'".$v."'"; else $param=$param.",'".$v."'";  
				}
				if (!array_key_exists('urlget', $this->datosPeticion)) { 
					foreach ($this->datosPeticion as $key => $v) { $this->_postdata[$key]=htmlspecialchars_decode($v);}
				}
			}
			
			if (!$this->conectarDBpg()) { $this->mostrarRespuesta(json_encode($this->devolverError(1)),500); }
			//$query = "select plpg from CF.APIS where idtmtd='".$this->_postdata['mtd']."' and ver='".$this->_postdata['ver']."'";
			$query = "select plpg from CF.APIS where idtmtd='".$this->_postdata['mtd']."'";
			//echo 'query -> '.print_r($query ,true)."\n";
			$result = pg_query($query); 
			if (!$result) {	$this->mostrarRespuesta(json_encode($this->devolverError(2)), 404); }
			$row = pg_fetch_array($result, null, PGSQL_ASSOC);
			if ($row == "") { $this->mostrarRespuesta(json_encode($this->devolverError(2)), 404); }
			
			$query = 'select '.$row['plpg'];
			$query = str_replace('%p',$param,$query);
			//echo 'param -> '.print_r($param ,true)."\n";
			//echo 'query -> '.print_r($query ,true)."\n";
			$result = pg_query($query); 
			if (!$result) {	$this->mostrarRespuesta(json_encode($this->devolverError(3)), 403); }
			$row = pg_fetch_array($result, null, PGSQL_NUM);
			$obj = json_decode($row[0]);
			//echo 'obj -> '.print_r($obj->{'code'} ,true)."\n";
			if ($obj->{'code'}<>'0') $this->mostrarRespuesta($row[0],$obj->{'http_code'}); 
			
			//echo 'idvisita -> '.print_r($obj->{'data'}->{'idvisita'} ,true)."\n";
			//echo 'mtd -> '.print_r(substr(strtoupper($mtd),0,8),true)."\n";
			//if (!isset($_SESSION['token']) and substr(strtoupper($mtd),0,8)=='AUTHUSER') 
			if (substr(strtoupper($mtd),0,8)=='AUTHUSER') $_SESSION['visita'] = $obj->{'data'}->{'idvisita'};
			//echo '$_SESSION[visita] -> '.print_r($_SESSION['visita'] ,true)."\n";
			if ($_SESSION['visita']==0) $this->mostrarRespuesta(json_encode($this->devolverError(0)), 400);
			$this->mostrarRespuesta($row[0],$obj->{'http_code'});
		}

	}

	$api = new Api();
	$api->procesarLlamada();
?>
