<?php
	ini_set("session.use_only_cookies","1");
	ini_set("session.use_trans_sid","0");
	header('Content-Type: application/json');
	session_start();
	
	require_once('rest.php');

	class Api extends Rest {
		//--------------------------------------------------------------------------------------------------------------
		//private $_cnxdata = array('usr'=>'app','psw'=>'app','host'=>'pbayax03.aguakan.mx:1522/x7agk');
		private $_cnxdata = array('usr'=>'app','psw'=>'app','host'=>'x7rptyax01.aguakan.mx:1522/x7agk');
		//private $_cnxdata = array('usr'=>'app','psw'=>'app2017','host'=>'koon.aguakan.mx:1521/x7agk');
		//--------------------------------------------------------------------------------------------------------------
		private $_cnxdatamy = array('user'=>'api','pssw'=>'Miyo.1405','host'=>'localhost','dbname'=>'','port'=>3306,'socket'=>'' );
		//--------------------------------------------------------------------------------------------------------------
		//private $_cnxdatapg = 'host=172.30.30.15 dbname=postgres user=postgres password=4gu4k4n#20 port=5432';
		private $_cnxdatapg = 'host=localhost dbname=postgres user=postgres password=miapao port=5433';
		//--------------------------------------------------------------------------------------------------------------
		private $_mtdauth = array('logincnt'=>'pbayax03.aguakan.mx:1522/x7agk',  'loginrnx'=>'pbayax03.aguakan.mx:1522/x7agk');
		//private $_mtdauth = array('logincnt'=>'x7rptyax01.aguakan.mx:1522/x7agk','loginrnx'=>'x7rptyax01.aguakan.mx:1522/x7agk');
		//private $_mtdauth = array('logincnt'=>'koon.aguakan.mx:1521/x7agk',  'loginrnx'=>'koon.aguakan.mx:1521/x7agk');
		//--------------------------------------------------------------------------------------------------------------
		private $_conn = null;

		public function __construct() {
			parent::__construct();
		}

		public function conectarDB() {
			$this->_conn = oci_connect($this->_cnxdata['usr'],$this->_cnxdata['psw'],$this->_cnxdata['host']);
			if (!$this->_conn) return false; else return true;
		}

		public function conectarDBpg() {
			$this->_conn = pg_connect($this->_cnxdatapg); // or die('No se ha podido conectar: ' . pg_last_error());
			if (!$this->_conn) return false; else return true;
		}

		public function conectarDBmy() {
			$this->_conn = new mysqli($this->_cnxdatamy['host'],$this->_cnxdatamy['user'],$this->_cnxdatamy['pssw'],$this->_cnxdatamy['dbname'],$this->_cnxdatamy['port'],$this->_cnxdatamy['socket'])
				or die ('Could not connect to the database server' . mysqli_connect_error());			
			if (!$this->_conn) return false; else return true;
		}

		private function devolverError($id) {
			$errores = array(
				array('code' => 'API_NO_ACEPTADO',			'http_code' => 400, 'info' => 'Solicitud no aceptada, no se establece el requerimiento para la demanda de servicios.'),
				array('code' => 'API_SOLICITUD_INCOMPLETA',	'http_code' => 400, 'info' => 'Solicitud incompleta, no se tienen los elementos necesarios para su demanda.'),
				array('code' => 'API_SIN_CONEXIONDB', 		'http_code' => 500, 'info' => 'Sin conexión a la Base de Datos, no responde.'),
				array('code' => 'API_SERVICIO_NO_EXISTE', 	'http_code' => 404, 'info' => 'El servicio no existe o ha expirado su acceso.'),
				array('code' => 'API_SERVICIO_NO_DISPONIBLE','http_code' => 403, 'info' => 'El servicio no se encuentra disponible en este momento, no responde.'),

				array('code' => 'API01', 'asunto' => 'petición de método no aceptada', 'mensaje' => 'Petición incorrecta para la demanda del servicio.'),
				array('code' => 'API04', 'asunto' => 'El token del servicio es inválido', 'mensaje' => 'No cumple el formato para ser reconocido.'),
				array('code' => 'API05', 'asunto' => 'La autenticación del usuario es inválido', 'mensaje' => 'No se pudo autenticar la demanda de los servicios a la Base de Datos.'),
				array('code' => 'API07', 'asunto' => 'Usuario y/o password incorrecto', 'mensaje' => 'Acceso inválido.'),
				);
			return $errores[$id];
		}

		private function getIP()
		{
			if (isset($_SERVER["HTTP_CLIENT_IP"]))	{ return $_SERVER["HTTP_CLIENT_IP"]; }
			elseif (isset($_SERVER["HTTP_X_FORWARDED_FOR"]))	{ return $_SERVER["HTTP_X_FORWARDED_FOR"]; }
			elseif (isset($_SERVER["HTTP_X_FORWARDED"]))		{ return $_SERVER["HTTP_X_FORWARDED"]; }
			elseif (isset($_SERVER["HTTP_FORWARDED_FOR"]))		{ return $_SERVER["HTTP_FORWARDED_FOR"]; }
			elseif (isset($_SERVER["HTTP_FORWARDED"]))			{ return $_SERVER["HTTP_FORWARDED"]; }
			else { return $_SERVER["REMOTE_ADDR"]; }
		}
		
		private function get_public_ip(){
			$externalContent = file_get_contents('http://checkip.dyndns.com/');
			preg_match('/Current IP Address: \[?([:.0-9a-fA-F]+)\]?/', $externalContent, $m);
			$externalIp = $m[1];
			return $externalIp;
		}
	
		public function procesarLlamada() {			
			if (!isset($_REQUEST['urlget']))	//identifica la existencia de la variable urlget
				$this->mostrarRespuesta(json_encode($this->devolverError(0)), 400);
			//si por ejemplo pasamos explode('/','////controller///method////args///') el resultado es un array con elem vacios;
			//Array ( [0] => [1] => [2] => [3] => [4] => controller [5] => [6] => [7] => method [8] => [9] => [10] => [11] => args [12] => [13] => [14] => )
			$url = trim($_REQUEST['urlget']);
			if (substr($url,0,1)=='/') $url = substr($url,1);
			if (substr($url,strlen($url)-1)=='/') $url=substr($url,0,strlen($url)-1);
			//se establece un arreglo con el contenido de url
			$url = explode('/',$url);  
			//con array_filter() filtramos elementos de un array pasando función callback, que es opcional.
			//si no le pasamos función callback, los elementos false o vacios del array serán borrados
			//por lo tanto la entre la anterior función (explode) y esta eliminammethod_existsiapos los '/' sobrantes de la URL
			$url = array_filter($url);		//un arreglo de indice numérico
			//echo 'url -> '.print_r($url,true)."\n";	
			$mtd = strtolower(array_shift($url));
			$ver = strtolower(array_shift($url));
			$argumentos = $url;
			//echo 'argumentos -> '.print_r($url,true)."\n";	
			//echo 'mtd -> '.print_r($mtd,true)."\n";	
			//echo 'ver -> '.print_r($ver,true)."\n";	
			//if ($mtd='authpro') {}			
			if (!isset($_SESSION['auth'])) $_SESSION['auth'] = null;	// la autenticación de la sesión
			$cambiocnx=false;
			if (array_key_exists('loginuser',$this->datosPeticion)) {
					//$jtex=json_encode($list[0],JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES | JSON_NUMERIC_CHECK);
				if (!array_key_exists('loginpassword',$this->datosPeticion) or
					!array_key_exists('coordx',$this->datosPeticion) or
					!array_key_exists('coordy',$this->datosPeticion))	
					$this->mostrarRespuesta(json_encode($this->devolverError(1)), 400);
				$ip = $this->getIP();
				//echo 'ip -> '.print_r($ip,true)."\n";
				if (strlen($ip)<6) {
					$theExternalIP = $this->get_public_ip();
					//echo 'theExternalIP -> '.print_r($theExternalIP,true)."\n";
					//$realIP = file_get_contents("http://ipecho.net/plain");
					//echo 'realIP -> '.print_r($realIP ,true)."\n";
					$ip=$theExternalIP;
				}
				//$hostname = gethostbyaddr($ip);			
				//if (!isset($_SESSION['coordx'])) $_SESSION['coordx'] = '0';
				//if (!isset($_SESSION['coordy'])) $_SESSION['coordy'] = '0';
				//if (!empty($this->datosPeticion['coordx']))	$_SESSION['coordx']	= $this->datosPeticion['coordx'];
				//if (!empty($this->datosPeticion['coordy']))	$_SESSION['coordy']	= $this->datosPeticion['coordy'];
				$this->_postdata = array('mtd' 		=> $mtd,
										 'ver' 		=> $ver,
										 'usuario'	=> $this->datosPeticion['loginuser'],
										 'password'	=> $this->datosPeticion['loginpassword'],
										 'ip'		=> $ip,
										 'coorx'	=> $this->datosPeticion['coordx'],
										 'coory'	=> $this->datosPeticion['coordy']
										 );
				//$this->_cnxdata['usr']=$this->datosPeticion['loginuser'];
				//$this->_cnxdata['psw']=$this->datosPeticion['loginpassword'];
				//$this->_cnxdata['host']= $this->_mtdauth[$this->datosPeticion['mtd']];
				//echo '_cnxdata -> '.print_r($this->_cnxdata,true)."\n";
				//echo '_postdata -> '.print_r($this->_postdata,true)."\n";
				//echo 'datosPeticion -> '.print_r($this->datosPeticion,true)."\n";
				$cambiocnx=true;
				$param='';
				foreach ($this->_postdata as $key => $v) { 
					$this->_postdata[$key]=htmlspecialchars_decode($v);
					if ($key =='mtd' or $key=='ver') continue;
					if (empty($param)) $param="'".$v."'"; else $param=$param.",'".$v."'"; 
				}
				//echo 'param -> '.print_r($param,true)."\n";
			} else {
				//$this->_postdata = array('api' => $api, 'ver' => $ver, 'mtd' => $mtd, 'ip' => $ip, 'auth' => $_SESSION['auth']);
				//$this->_postdata = array('api' => $api, 'ver' => $ver, 'mtd' => $mtd, 'auth' => $_SESSION['auth']);
				$this->_postdata = array('mtd' => $mtd, 'ver' => $ver);
				//echo print_r($this->_postdata,true)."\n";	
				
				$i = 0; $param=''; 
				foreach ($argumentos as $v) { 
					$i++; $this->_postdata['prm'.$i]=$v; 
					if (empty($param)) $param="'".$v."'"; else $param=$param.",'".$v."'";  
				}
				if (!array_key_exists('urlget', $this->datosPeticion)) { 
					foreach ($this->datosPeticion as $key => $v) { $this->_postdata[$key]=htmlspecialchars_decode($v);}
				}
			}
			
			//echo print_r($param,true)."\n";	
			//echo print_r($this->_postdata,true)."\n";	
		
					
					//hasta aqui web.php
					//if (!$cambiocnx)
						//if 	(!array_key_exists('auth',$this->datosPeticion) and !array_key_exists('keyorg',$this->datosPeticion)) 
							//$this->mostrarRespuesta(json_encode($this->devolverError(4)),401);
							
			if (!$this->conectarDBpg()) {
				$this->mostrarRespuesta(json_encode($this->devolverError(2)),500);
				//if ($cambiocnx)
					//$this->mostrarRespuesta(json_encode($this->devolverError(7)),401);
				//else
					//$this->mostrarRespuesta(json_encode($this->devolverError(6)),404);
				//return;
			}
			$query = "select plpg from CF.APIS where idtmtd='".$this->_postdata['mtd']."' and ver='".$this->_postdata['ver']."'";
			//echo print_r($query,true)."\n";
			$result = pg_query($query); 
			//echo 'result -> '.print_r($result,true)."\n";
			if (!$result) {	$this->mostrarRespuesta(json_encode($this->devolverError(3)), 404); }
			//echo 'tuvo resultado...';
	//while ($row = pg_fetch_row($result)) {
        //if ($first) {
            //$names = $row[0];
            //$ids = $row[1];
            //$first = false;
        //} else {
            //$names = $names . " " . $row[0];
            //$ids = $ids . " " . $row[1];
        //}
    //}
    //if ($ids == "") {
        //echo "denied, empty result";
    //}

			$row = pg_fetch_array($result, null, PGSQL_ASSOC);
			//echo 'row -> '.print_r($row,true)."\n";	
			if ($row == "") { $this->mostrarRespuesta(json_encode($this->devolverError(3)), 404); }
			//echo 'continua...';
			$query = 'select '.$row['plpg'];
			//echo print_r($query,true)."\n";	
			//echo print_r($param,true)."\n";					
			$query = str_replace('%p',$param,$query);
			//echo print_r($query,true)."\n";	
			$result = pg_query($query); 
			if (!$result) {	$this->mostrarRespuesta(json_encode($this->devolverError(4)), 403); }
			$row = pg_fetch_array($result, null, PGSQL_NUM);
			//echo 'row -> '.print_r($row,true)."\n";	
			$obj = json_decode($row[0]);
			//echo 'obj -> '.print_r($obj->{'code'},true)."\n";	
			if (empty($obj->{'code'})) { $this->mostrarRespuesta(json_encode($this->devolverError(4)), 403); }
			$http= $obj->{'http_code'}; 
			$this->mostrarRespuesta($row[0],$http);
		}

	}

	$api = new Api();
	$api->procesarLlamada();
?>
