<?php
	require_once('cer.php');
	class Rest extends Cerpro {
		public $tipo = "application/json";
		public $datosPeticion = array();
		private $_codEstado = 200;

		public function __construct() {
		 $this->tratarEntrada();
		}

		private function setCabecera() {
			header("HTTP/1.1 " . $this->_codEstado . " " . $this->getCodEstado());
			header("Content-Type:" . $this->tipo . ';charset=utf-8');
		}

		public function mostrarRespuesta($data, $estado) {
			$this->_codEstado = ($estado) ? $estado : 200;//si no se envía $estado por defecto será 200
			$this->setCabecera();
			echo $data;
			exit;
		}

		private function limpiarEntrada($data) {
			$entrada = array();
			if (is_array($data)) {
				foreach ($data as $key => $value) {
					$entrada[$key] = $this->limpiarEntrada($value);
				}
			} else {
				if (get_magic_quotes_gpc()) {
					//Quitamos las barras de un string con comillas escapadas
					//Aunque actualmente se desaconseja su uso, muchos servidores tienen activada la extensión magic_quotes_gpc.
					//Cuando esta extensión está activada, PHP añade automáticamente caracteres de escape (\) delante de las comillas que se escriban en un campo de formulario.
					$data = trim(stripslashes($data));
				}
				//eliminamos etiquetas html y php
				$data = strip_tags($data);
				//Conviertimos todos los caracteres aplicables a entidades HTML
				$data = htmlentities($data);
				$entrada = trim($data);
			}
			return $entrada;
		}

		private function tratarEntrada() {
			$metodo = $_SERVER['REQUEST_METHOD'];
			switch ($metodo) {
				case "GET":
					$this->datosPeticion = $this->limpiarEntrada($_GET);
					break;
				case "POST":
					$this->datosPeticion = $this->limpiarEntrada($_POST);
					break;
				case "DELETE"://"falling though". Se ejecutará el case siguiente
				case "PUT":
					//php no tiene un método propiamente dicho para leer una petición PUT o DELETE por lo que se usa un "truco":
					//leer el stream de entrada file_get_contents("php://input") que transfiere un fichero a una cadena.
					//Con ello obtenemos una cadena de pares clave valor de variables (variable1=dato1&variable2=data2...)
					//que evidentemente tendremos que transformarla a un array asociativo.
					//Con parse_str meteremos la cadena en un array donde cada par de elementos es un componente del array.
					parse_str(file_get_contents("php://input"), $this->datosPeticion);
					$this->datosPeticion = $this->limpiarEntrada($this->datosPeticion);
					break;
				default:
					$this->response('', 404);
					break;
			}
		}

		private function getCodEstado() {
			$estado = array(
			//2xx  Éxito -> Success
			200 => 'OK',					// Everything worked as expected.
			201 => 'Created',				// La solicitud se ha cumplido y se ha creado un nuevo recurso.
			202 => 'Accepted',				// La solicitud ha sido aceptada para su procesamiento, pero el procesamiento no se ha completado. 
			204 => 'No Content',			// El servidor procesó con éxito la solicitud, pero no devuelve ningún contenido.
			//3xx  Redireccionamiemto
			301 => 'Moved Permanently',		// Al recurso solicitado se le ha asignado un nuevo URI permanente y cualquier referencia futura a este recurso DEBERÍA usar uno de los URI devueltos
			302 => 'Found',					// El recurso solicitado reside temporalmente bajo un URI diferente. Debido a que la redirección puede ser alterada ocasionalmente, el cliente DEBE continuar usando el URI de solicitud para futuras solicitudes.
			303 => 'See Other',				// La respuesta a la solicitud se puede encontrar en un URI diferente y DEBE recuperarse utilizando un método GET en ese recurso.
			304 => 'Not Modified',			// Si el cliente ha realizado una solicitud GET condicional y se permite el acceso, pero el documento no se ha modificado, el servidor DEBE responder con este código de estado
			//4xx  Error del Cliente
			400 => 'Bad Request',			// **The request was unacceptable, often due to missing a required parameter. -> La solicitud no fue aceptada, probablemente por la falta de parámetros requeridos.
			401 => 'Unauthorized',			// **No valid API key provided.  -> Similar a 403 Prohibido, pero específicamente para usar cuando la autenticación es posible pero ha fallado o aún no se ha proporcionado. 
			402 => 'Request Failed',		// The parameters were valid but the request failed.
			403 => 'Forbidden',				// **The API key doesn't have permissions to perform the request.  -> Código de error para el usuario no autorizado para realizar la operación o el recurso no está disponible por alguna razón (por ejemplo, restricciones de tiempo, etc.)
			404 => 'Not Found',				// **The requested resource doesn't exist.  -> Se usa cuando no se encuentra el recurso solicitado, si no existe o si hubo un 401 o 403 que, por razones de seguridad, el servicio desea enmascarar.
			405 => 'Method Not Allowed',	// Se realizó una solicitud de un recurso utilizando un método de solicitud no admitido por ese recurso; por ejemplo, usar GET en un formulario que requiere que los datos se presenten a través de POST, o usar PUT en un recurso de solo lectura.
			409 => 'Conflict',				// The request conflicts with another request (perhaps due to using the same idempotent key). -> Indica que la solicitud no se pudo procesar debido a un conflicto en la solicitud, como un conflicto de edición.
			429 => 'Too Many Requests',		// Too many requests hit the API too quickly. We recommend an exponential backoff of your requests. -> El usuario ha enviado demasiadas solicitudes en un período de tiempo determinado. Diseñado para su uso con esquemas de limitación de velocidad.
			//5xx  Error en el Servidor
			500 => 'Internal Server Error');// **Something went wrong on Stripe's end. (These are rare.) ->  Un mensaje de error genérico, dado cuando no es adecuado un mensaje más específico.  El error general general cuando el lado del servidor arroja una excepción
			$respuesta = ($estado[$this->_codEstado]) ? $estado[$this->_codEstado] : $estado[500];
			return $respuesta;
		}
	}
?>
