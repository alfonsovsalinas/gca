<?php
/*
*	Nombre: CriptoKey
*	Descripción:
*		Clase que permite encriptar y desencriptar una cadena de caracteres escrita con caracteres ANSII.
*		Utiliza el método de criptografía simétrica (en inglés symmetric key cryptography). En el cual se usa una misma clave para cifrar y descifrar mensajes.
*		Las dos partes que se comunican han de ponerse de acuerdo de antemano sobre la clave a usar. Una vez que ambas partes tienen acceso a esta clave,
*		el remitente cifra un mensaje usando la clave, lo envía al destinatario, y éste lo descifra con la misma clave.
*	Descripción de uso:
*		Para crear las llaves se usa el método  $this->CreaLlaves() indicando el nombre con el cual creara el archivo.
*			$cripto = new EncriptaAnsiiSimetrico;
*			$cripto->NombreLLave="sistemas";
*			$cripto->CreaLlaves();
*		Para encriptar una cadena se usa el método $this->Encriptar(), el cual requiere se defina la cadena a encriptar ($this->cadena)
*		Para desencriptar una cadena encriptada se  usa el método $this->Desencriptar(), el cual requiere se definan la cadena encriptada $this->cadenaEcrip y
*		se indique el mismo archivo llave que se usó para encriptar.
*			$cripto->cadenaEcrip=$Encriptado;
*			echo $cripto->Desencriptar();
*	Autor: Tomado de referencia idea de Agustin Rios Reyes
*/

class CriptoKey{
	public  $keytis;
	public  $cadkeytis;
	public  $keyorg;
	public  $cadkeyorg;
	public  $cadena;
	public  $cadenaCripto;
	public  $archivocer;
	public  $archivokey;

	/*
	*	Método que crea la llave para encriptar y desencriptar del administrador TI
	*	Parámetros:
	*		- $pkeytis: define la cadena que será empleada de referencia para el cálculo de la llave.
	*	Salida:
	*		- La llave encriptada de 32 caracteres que será utilizada para aplicar encriptamiento en los contenidos
	*/
	public function keytis() {
		if (!empty($this->cadkeytis))
			$this->keytis=md5(sha1($this->cadkeytis));
		else
			$this->keytis=null;
		return $this->keytis; //99b8536a280538b75cbe54e472b2cee0
	}

	/*
	*	Método que crea la llave para encriptar y desencriptar para la organización o centro operacional
	*	Parámetros:
	*		- $pcadorg: define la cadena JSON que será empleada de referencia para el cálculo de la llave. Se requiere contenido encriptado de la llave $keytis
	*	Salida:
	*		- La llave encriptada de 32 caracteres que será utilizada para aplicar encriptamiento en los contenidos
	*/
	public function keyorg() {
		if (strlen($this->keytis)==32) {
			$this->cadkeyorg = strtr($this->cadkeyorg, array('}' => ',"keytis":"'.$this->keytis.'"}'));
			$this->keyorg = md5(sha1($this->cadkeyorg));
		} else {
			$this->keyorg = null;
		}
		return $this->keyorg;
	}

	/*
	*	Método publico que se usa para mostrar la cadena Encriptada.
	*	Parámetros:
	*		- $this->cadena: cadena a encriptar.
	*	Salida:
	*		_ Devuelve la cadena encriptada.
	*/
	public function Encripta() {
		$this->cadena=utf8_decode($this->cadena);
		if (!empty($this->keyorg)) {
			$this->cadenaCripto=base64_encode(mcrypt_encrypt(MCRYPT_RIJNDAEL_256, $this->keyorg, $this->cadena, MCRYPT_MODE_CBC, $this->keyorg));
		} else {
			$this->cadenaCripto=null;
		}
		return $this->cadenaCripto;
	}
	/*
	*	Método publico que se usa para mostrar la cadena  Desencriptada.
	*	Parámetros:
	*		- $this->cadenaEcrip: cadena a desencriptar.
	*	Salida:
	*		_ Devuelve la cadena desencriptada y legible.
	*/
	public function Desencripta() {
		$this->cadenaCripto=utf8_decode($this->cadenaCripto);
		$decrypted=null;
		if (!empty($this->key)) {
			$decrypted = rtrim(mcrypt_decrypt(MCRYPT_RIJNDAEL_256, $this->key, base64_decode($this->cadenaCripto), MCRYPT_MODE_CBC, $this->key), "\0");
		}
		return $decrypted;
	}
}
?>
