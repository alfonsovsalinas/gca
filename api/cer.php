<?php
class Cerpro {
/*
*	Nombre: Cerpro (CriptoKey)
*	Descripción:
*		Clase que permite encriptar y desencriptar una cadena de caracteres escrita con caracteres ANSII.
*		Utiliza el método de criptografía simétrica (en inglés symmetric key cryptography). En el cual se usa una misma clave para cifrar y descifrar mensajes.
*		Las dos partes que se comunican han de ponerse de acuerdo de antemano sobre la clave a usar. Una vez que ambas partes tienen acceso a esta clave,
*		el remitente cifra un mensaje usando la clave, lo envía al destinatario, y éste lo descifra con la misma clave.
*	Descripción de uso:
*		Para crear las llaves se usa el método  $this->CreaLlaves() indicando el nombre con el cual creara el archivo.
*			$cripto = new EncriptaAnsiiSimetrico;
*			$cripto->NombreLLave="sistemas";
*			$cripto->CreaLlaves();
*		Para encriptar una cadena se usa el método $this->Encriptar(), el cual requiere se defina la cadena a encriptar ($this->cadena)
*		Para desencriptar una cadena encriptada se  usa el método $this->Desencriptar(), el cual requiere se definan la cadena encriptada $this->cadenaEcrip y
*		se indique el mismo archivo llave que se usó para encriptar.
*			$cripto->cadenaEcrip=$Encriptado;
*			echo $cripto->Desencriptar();
*	Autor: Tomado de referencia idea de Agustin Rios Reyes
*/

	public  $cadena;					// La cadena que se va a encriptar
	public  $keymd;						// para el resultado de encriptamiento md5
	public  $keyorg =	'AGUAKAN';		// Llave de la organización para el encritamiento y des-encriptamiento
	public  $cadenaCripto;				// La cadena que se va a desencriptar
	public	$certificado = array(
			'certificado'		=>	'eyJXXXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOi8vYXBpLm1lbnNhZgghLmRldi92MS9sb2dpbiIsImlhdCI6MTQ2NDM1NDY5OSwiZXhwIjoxNDY0MzU4Mjk5LCJuYmYisfE0NjQzNTQ2OTksImp0aSI6IjIyNDg4Y2IxM2RkNzZlODZjM2NhZWZhZjNhMDBkMjkzIiwic3ViIjoxNH0.F3q4ckNbI8sMg9RX_iRSyrEmGWW3oyO8dMcasKl5xer',
			'razon_social' 		=>	'DESARROLLOS HIDRAULICOS DE CANCUN, S.A. DE C.V.',
			'nombre_comercial'	=>	'AGUAKAN',
			'max_contratos'		=>	'9999999',
			'expira_producto'	=>	'31/12/2022',
			'expira_soporte'	=>	'31/12/2021'
			);

	/*
	*	Método que crea la llave encriptada md5
	*	Parámetros:
	*		- $cadena: define la cadena que será empleada de referencia para el cálculo de la llave.
	*	Salida:
	*		- La llave encriptada de 32 caracteres que será utilizada para aplicar encriptamiento en los contenidos
	*/
	public function keymd5() {
		if (!empty($this->cadena))
			$this->keymd=md5(sha1($this->cadena));
		else
			$this->keymd=null;
		return $this->keymd; //99b8536a280538b75cbe54e472b2cee0
	}

	/*
	*	Método publico que se usa para generar una cadena Encriptada de base64.
	*	Parámetros:
	*		- $this->cadena: cadena a encriptar.
	*	Salida:
	*		- Devuelve la cadena encriptada.
	*/
	public function Encripta() {
		$this->cadena=utf8_decode($this->cadena);
		if (!empty($this->keyorg)) {
			$this->cadenaCripto=base64_encode(mcrypt_encrypt(MCRYPT_RIJNDAEL_256, $this->keyorg, $this->cadena, MCRYPT_MODE_CBC, $this->keyorg));
		} else {
			$this->cadenaCripto=null;
		}
		return $this->cadenaCripto;
	}
	
	/*
	*	Método publico que se usa para mostrar la cadena  Desencriptada.
	*	Parámetros:
	*		- $this->cadenaCripto: cadena a desencriptar.
	*	Salida:
	*		- Devuelve la cadena desencriptada y legible.
	*/
	public function Desencripta() {
		$this->cadenaCripto=utf8_decode($this->cadenaCripto);
		$decrypted=null;
		if (!empty($this->key)) {
			$decrypted = rtrim(mcrypt_decrypt(MCRYPT_RIJNDAEL_256, $this->keyorg, base64_decode($this->cadenaCripto), MCRYPT_MODE_CBC, $this->keyorg), "\0");
		}
		return $decrypted;
	}
}
?>
